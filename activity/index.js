/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	// 1st Info
	// String Type
	let fName = 'John';
	let lName = "Smith";
	let currentAge = 30;
	let hb1 = 'Biking';
	let hb2 = 'Mountain Climbing';
	let hb3 = 'Swimming';

	let	firstName = 'First Name: ' + fName;
	console.log(firstName);
	let	lastName = 'Last Name: ' + lName;
	console.log(lastName);
	let age = "Age: " + currentAge;
	console.log(age);

	// Array Type
	let hobbies=[hb1 , hb2, hb3];
	console.log("Hobbies: ")
	console.log (hobbies);

	// Object Type
	let workAddress = {
	houseNumber : '32',
	street : 'Washington',
	city : 'Lincoln',
	state : 'Nebraska',
	}
	console.log("Work Address:")
	console.log(workAddress);

	// 2nd Info

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge2 = 40;
	console.log("My current age is: " + currentAge2);


	// String Type 
	let f1 = 'Tony';
	let f2 = 'Bruce';
	let f3 = 'Thor'; 
	let f4 = 'Natasha'; 
	let f5 = 'Clint'; 
	let f6 = 'Nick';    

	// Array Type
	let friends = [f1, f2, f3, f4,f5, f6];
	console.log("My Friends are:")
	console.log(friends);

	// Object Type
	let profile = {
		username: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,
	}
	console.log("My Full Profile: ")
	console.log(profile);

	
	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);